#!/bin/bash

minikube start --kubernetes-version='1.11.10'
kubectl create -f ./minio_nodeport.yaml
kubectl create namespaces spinnaker