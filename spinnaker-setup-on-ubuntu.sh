#!/bin/bash
# Author: Verendra Kakumanu
# Description: Installs spinnaker on ubuntu server
# Please spcify below configuration details and run the script.
#

USERNAME="veerendra080542"
REPOSITORIES="veerendra080542/php-mysql"
ADDRESS="quay.io"
MINIO_SECRET_KEY="bloopark"
MINIO_ACCESS_KEY="bloopark"
KUBECONFIG_FILE="~/.kube/spinnaker-config"
ENDPOINT="192.168.99.100:31372"
SPINNAKER_UI="0.0.0.0" #IP addess of ubuntu server which currently your deploying spinnaker


echo "Installing kubeclt binary"
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.8.0/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
echo "[*] Installing Halyard"
#curl -O https://raw.githubusercontent.com/spinnaker/halyard/master/install/macos/InstallHalyard.sh # MAC
curl -O https://raw.githubusercontent.com/spinnaker/halyard/master/install/stable/InstallHalyard.sh # Ubuntu
sudo bash InstallHalyard.sh
hal config deploy edit --type localdebian
echo $MINIO_SECRET_KEY | hal config storage s3 edit --endpoint $ENDPOINT --access-key-id $MINIO_ACCESS_KEY --secret-access-key
hal config storage edit --type s3
hal config provider docker-registry enable
hal config provider docker-registry account add my-docker-registry --address $ADDRESS --repositories $REPOSITORIES --username $USERNAME --password
hal config provider kubernetes enable
hal config provider kubernetes account add my-k8s-account --docker-registries my-docker-registry --kubeconfig-file $KUBECONFIG_FILE
hal version list 
read version
if [ -z "$version" ]
then
	echo ""
else
	echo $version>version.tmp
	break
fi
done
hal config version edit --version `cat version.tmp`
echo "host: 0.0.0.0" | tee \
    ~/.hal/default/service-settings/gate.yml \
    ~/.hal/default/service-settings/deck.yml
hal config security ui edit --override-base-url http://$SPINNAKER_UI:9000 #Deck
hal config security api edit --override-base-url http://$SPINNAKER_UI:8084 #Gate
hal deploy apply
